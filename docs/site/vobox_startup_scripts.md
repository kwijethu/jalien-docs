# JAliEn Startup Scripts

JAliEn Startup Scripts are used to control CE and MonaLisa services in VOboxes. The scripts will reside in `/cvmfs/alice.cern.ch/scripts/vobox`

# Usage
``` 
Usage: jalien-vobox <Command> [<Service>]

<Command> is one of: start status stop restart mlstatus

<Service> is one of: ce monalisa (defaulting to both if not specified)
```

Running a command without specifying a service will run the command on both CE and MonaLisa services.

## Configuration
Both the services are able to start without any configurations. However, the following parameters can be overridden by defining them in `$HOME/.alien/config/version.properties` as key-value pairs.

```
MONALISA_HOME=<MonaLisa package location>
MONALISA=<MonaLisa version>
JALIEN=<JAliEn version>
```

> Note: as the MonaLisa package is not available in CVMFS yet, for now one can define MONALISA_HOME to specify the MonaLisa package location. If it is not locally available, please use `install` as the value for `<MonaLisa package location>`.

> Note: the JAliEn MonaLisa package is not yet ready for widespread use, as it lacks a few features that are important for operations.

If it is required to run any shell command before starting either of the services, they can be added in `$HOME/.alien/config/CE.env` or `$HOME/.alien/config/ml.env`. The former should typically contain a command to define the `X509_USER_PROXY` that should be used by the JAliEn CE to submit its jobs, for example as follows:

```
export X509_USER_PROXY=`ls -t /var/lib/vobox/alice/proxy_repository/*lcgadmin | sed q`
```

