# jalien Commands

## **cp**

Options for this command are client-specific. Refer to the relevant client usage as shown below.

=== "Usage: jalien"
    ```console
    
    usage: cp   [-options] < file:///localfile /gridfile >  |  < /gridfile file:///localfile >  |  < -t /gridfile >
    
    options:
                        -g                     :  get by GUID
                        -S                     :  [se[,se2[,!se3[,qos:count]]]]
                        -t                     :  create a local temp file
                        -silent                :  execute command silently
                        -w                     :  wait for all replicas to complete upload before returning (default false)
                        -W                     :  do _not_ wait for all replicas to complete upload, return as soon as the first replica is available
                        -T                     :  Use this many concurrent threads (where possible) - default 1
                        -d                     :  delete local file after a successful upload (i.e. move local to Grid)
                        -j                     :  the job ID that has created the file
    ```

=== "Usage: alien.py"
    ```console
    at least 2 arguments are needed : src dst
    the command is of the form of (with the strict order of arguments): cp <args> src dst
    where src|dst are local files if prefixed with file:// or file: or grid files otherwise
    after each src,dst can be added comma separated specifiers in the form of: @disk:N,SE1,SE2,!SE3
    where disk selects the number of replicas and the following specifiers add (or remove) storage endpoints from the received list
    args are the following :
    -h : print help
    -f : replace destination file (if destination is local it will be replaced only if integrity check fails)
    -P : enable persist on successful close semantic
    -cksum : check hash sum of the file; for downloads the central catalogue md5 will be verified; for uploads (for new enough xrootds) a hash type will be negociated with remote and transfer will be validated
    -y <nr_sources> : use up to the number of sources specified in parallel
    -S <aditional TPC streams> : uses num additional parallel streams to do the transfer. (max = 15)
    -chunks <nr chunks> : number of chunks that should be requested in parallel
    -chunksz <bytes> : chunk size (bytes)
    -T <nr_copy_jobs> : number of parralel copy jobs from a set (for recursive copy)
    
    for the recursive copy of directories the following options (of the find command) can be used:
    -glob <globbing pattern> : this is the usual AliEn globbing format; N.B. this is NOT a REGEX!!! defaults to all "*"
    -select <pattern> : select only these files to be copied; N.B. this is a REGEX applied to full path!!!
    -name <pattern> : select only these files to be copied; N.B. this is a REGEX applied to a directory or file name!!!
    -name <verb>_string : where verb = begin|contain|ends|ext and string is the text selection criteria.
    verbs are aditive : -name begin_myf_contain_run1_ends_bla_ext_root
    N.B. the text to be filtered cannont have underline <_> within!!!
    -parent <parent depth> : in destination use this <parent depth> to add to destination ; defaults to 0
    -a : copy also the hidden files .* (for recursive copy)
    -j <queue_id> : select only the files created by the job with <queue_id>  (for recursive copy)
    -l <count> : copy only <count> nr of files (for recursive copy)
    -o <offset> : skip first <offset> files found in the src directory (for recursive copy)
    ```

=== "Example"
    ```console
    jsh: [alice] > cp file://example.file example.file
    ```

## **quota**

Options for this command are client-specific. Refer to the relevant client usage as shown below.

=== "Usage: alien.py"
    ```console
    Client-side implementation that make use of server's jquota and fquota (hidden by this implementation)
    Command format: quota [user]
    if [user] is not provided, it will be assumed the current user
    ```

## **cd**

=== "Usage"
    ```console
    
    usage: cd   [dir]   
    ```

=== "Example"
    ```console
    jsh: [alice] > cd /alice/cern.ch/user/u/username
    ```

## **pwd**

No help available for this command

=== "Example"
    ```console
    jsh: [alice] > pwd
    /alice/cern.ch/user/u/username/
    ```

## **mkdir**

=== "Usage"
    ```console
    
    usage: mkdir   [-options] <directory> [<directory>[,<directory>]]
    
    options:
                        -p                     :  create parents as needed
                        -silent                :  execute command silently
    ```

## **rmdir**

=== "Usage"
    ```console
    
    usage: rmdir    [<option>] <directory>
    
    options:
                        --ignore-fail-on-non-empty  :    ignore each failure that is solely because a directory is non-empty
                        -p                     :  --parents   Remove DIRECTORY and its ancestors.  E.g., 'rmdir -p a/b/c' is similar to 'rmdir a/b/c a/b a'.
                        -v                     :  --verbose  output a diagnostic for every directory processed
                                               :  --help      display this help and exit
                                               :  --version  output version information and exit
                        -silent                :  execute command silently
    ```

## **ls**

=== "Usage"
    ```console
    
    usage: ls   [-options] [<directory>]
    
    options:
                        -l                     :  long format
                        -a                     :  show hidden .* files
                        -F                     :  add trailing / to directory names
                        -b                     :  print in guid format
                        -c                     :  print canonical paths
                        -H                     :  human readable file sizes (1024-based); implies '-l'
    ```

=== "Example"
    ```console
    jsh: [alice] > ls -lFH example.file
    -rwxr-xr-x   username username         56 B Oct 08 10:35    example.file
    ```

## **find**

=== "Usage"
    ```console
    
    usage: find   [flags] <path> <pattern>
    
    
    options:
                        -a                     :  show hidden .* files
                        -s                     :  no sorting
                        -c                     :  collection filename (put the output in a collection)
                        -y                     :  (FOR THE OCDB) return only the biggest version of each file
                        -x                     :  xml collection name (return the LFN list through XmlCollection)
                        -d                     :  return also the directories
                        -w[h]                  :  long format, optionally human readable file sizes
                        -j <queueid>           :  filter files created by a certain job
                        -l <count>             :  limit the number of returned entries to at most the indicated value
                        -o <offset>            :  skip over the first /offset/ results
                        -r                     :  pattern is a regular expression
                        -f                     :  return all LFN data as JSON fields (API flag only)
    ```

## **toXml**

=== "Usage"
    ```console
    
    usage: toXml   [-i] [-x xml_file_name] [-a] [-l list_from] [lfns]
    
    options:
                        -i                     :  ignore missing entries, continue even if some paths are not/no longer available
                        -x                     :  write the XML content directly in this target AliEn file
                        -a                     :  (requires -x) append to the respective collection
                        -l                     :  read the list of LFNs from this file, one path per line
    ```

=== "Example"
    ```console
    jsh: [alice] > toXml example.file
    <?xml version="1.0"?>
    <alien>
      <collection name="tempCollection">
        <event name="1">
          <file name="example.file" aclId="" broken="0" ctime="2020-10-08 10:35:24" dir="33531555" entryId="325149679" expiretime="" gowner="username" guid="35eada00-0941-11eb-8158-d8d385976ec8" guidtime="" jobid="" lfn="/alice/cern.ch/user/u/username/example.file" md5="b0b07c5bab5b5418250f8d63d3102726" owner="username" perm="755" replicated="0" size="56" turl="alien:///alice/cern.ch/user/u/username/example.file" type="f" />
        </event>
        <info command="example.file" creator="username" date="Fri Oct 09 22:24:15 CEST 2020" timestamp="1602275055178" />
      </collection>
    </alien>
    ```

## **cat**

=== "Usage"
    ```console
    
    usage: cat   [-options] [<filename>]
    
    options:
                        -o                     :  outputfilename
                        -n                     :  number all output lines
                        -b                     :  number nonblank output lines
                        -E                     :  shows ends - display $ at end of each line number
                        -T                     :  show tabs -display TAB characters as ^I
    ```

=== "Example"
    ```console
    jsh: [alice] > cat example.file
    Hello there dear user. This is just a simple text file.
    ```

## **whereis**

=== "Usage"
    ```console
    
    usage: whereis   [-options] [<filename>]
    
    options:
                        -g                     :  use the lfn as guid
                        -r                     :  resolve links (do not give back pointers to zip archives)
                        -l                     :  lookup the LFN of the ZIP archive (slow and expensive IO operation, only use it sparingly!)
    ```

=== "Example"
    ```console
    jsh: [alice] > whereis example.file
    the file example.file is in
    
    	 SE => ALICE::RAL::CEPH         pfn => root://alice.echo.stfc.ac.uk:1094/alice:/00/24641/35eada00-0941-11eb-8158-d8d385976ec8
    
    	 SE => ALICE::CERN::EOS         pfn => root://eosalice.cern.ch:1094//00/24641/35eada00-0941-11eb-8158-d8d385976ec8
    ```

## **rm**

=== "Usage"
    ```console
    
    usage: rm    <LFN> [<LFN>[,<LFN>]]
    
    options:
                        -f                     :  ignore nonexistent files, never prompt
                        -r, -R                 :  remove directories and their contents recursively
                        -i                     :  prompt before every removal (for JSh clients)
    ```

## **mv**

=== "Usage"
    ```console
    
    usage: mv    <LFN>  <newLFN>
    ```

## **touch**

=== "Usage"
    ```console
    
    usage: touch    <LFN> [<LFN>[,<LFN>]]
    ```

## **type**

No help available for this command

=== "Example"
    ```console
    jsh: [alice] > type example.file
    file
    ```

## **lfn2guid**

=== "Usage"
    ```console
    
    usage: lfn2guid   <filename>
    ```

=== "Example"
    ```console
    jsh: [alice] > lfn2guid example.file
    /alice/cern.ch/user/u/username/example.file                                     35eada00-0941-11eb-8158-d8d385976ec8
    ```

## **guid2lfn**

=== "Usage"
    ```console
    
    usage: guid2lfn   <GUID>
    ```

## **access**

=== "Usage"
    ```console
    
    usage: access   [options] <read|write> <lfn> [<specs>]
                        -s                     :  for write requests, size of the file to be uploaded, when known
                        -m                     :  for write requests, MD5 checksum of the file to be uploaded, when known
                        -j                     :  for write requests, the job ID that created these files, when applicable
    ```

## **commit**

No help available for this command

## **chgroup**

No help available for this command

## **chown**

=== "Usage"
    ```console
    
    Usage: chown -R <user>[.<group>] <file>
    
    Changes an owner or a group for a file
    -R : do a recursive chown
    ```

## **deleteMirror**

=== "Usage"
    ```console
    
    Removes a replica of a file from the catalogue
    Usage:
            deleteMirror [-g] <lfn> <se> [<pfn>]
    
    Options:
       -g: the lfn is a guid
    
    
    Removes a replica of a file from the catalogue
    Usage:
            deleteMirror [-g] <lfn> <se> [<pfn>]
    
    Options:
       -g: the lfn is a guid
    ```

## **md5sum**

=== "Usage"
    ```console
    
    usage: md5sum   <filename1> [<or guid>] ...
    ```

=== "Example"
    ```console
    jsh: [alice] > md5sum example.file
    b0b07c5bab5b5418250f8d63d3102726	example.file
    ```

## **mirror**

=== "Usage"
    ```console
    
    mirror Copies a file into another SE
     Usage:
    	mirror [-g] [-try <number>] [-S [se[,se2[,!se3[,qos:count]]]]] <lfn> [<SE>]
                     -g:      Use the lfn as a guid
                     -S:     specifies the destination SEs to be used
                     -try <NumOfAttempts>     Specifies the number of attempts to try and mirror the file
    ```

## **grep**

=== "Usage"
    ```console
    
    usage: grep   [-linux grep options] <pattern> [<filename>]+
    
    options:
    ```

=== "Example"
    ```console
    jsh: [alice] > grep Hello example.file
    Hello there dear user. This is just a simple text file.
    ```

## **changeDiff**

=== "Usage"
    ```console
    
    Show changes between the current version of the file and the previous one (same file name with a '~' suffix)
    usage: changeDiff   [<filename>]
    
    options:
    ```

## **listFilesFromCollection**

=== "Usage"
    ```console
    
    usage: listFilesFromCollection   [-options] collection
    
    options:
                        -z                     :  show size and other file details
                        -s                     :  silent (API only)
    ```

## **addFileToCollection**

No help available for this command

## **packages**

=== "Usage"
    ```console
    
    usage: packages     list available packages
    ```

## **submit**

=== "Usage"
    ```console
    
    usage: submit   <URL>
    
                        <URL> => <LFN>
                        <URL> => file:///<local path>
    ```

## **ps**

=== "Usage"
    ```console
    
    usage: ps   [-options]
    
    options:
                        -F l | -Fl | -L        :  long output format
                        -f <flags|status>    
                        -u <userlist>        
                        -s <sitelist>        
                        -n <nodelist>        
                        -m <masterjoblist>   
                        -o <sortkey>         
                        -j <jobidlist>       
                        -l <query-limit>     
    
                        -M                     :  show only masterjobs
                        -X                     :  active jobs in extended format
                        -A                     :  select all owned jobs of you
                        -W                     :  select all jobs which are waiting for execution of you
                        -E                     :  select all jobs which are in error state of you
                        -a                     :  select jobs of all users
                        -b                     :  do only black-white output
                        -jdl <jobid>           :  display the job jdl
                        -trace <jobid> <tag>*  :  display the job trace information (not working yet!)
    ```

## **masterjob**

=== "Usage"
    ```console
    
    usage: masterjob   <jobId> [-options] [merge|kill|resubmit|expunge]
    
    options:
                        -status <status>       :  display only the subjobs with that status
                        -id <id>               :  display only the subjobs with that id
                        -site <id>             :  display only the subjobs on that site
                        -printid               :  print also the id of all the subjobs
                        -printsite             :  split the number of jobs according to the execution site
    
                        merge                  :  collect the output of all the subjobs that have already finished
                        kill                   :  kill all the subjobs
                        resubmit               :  resubmit all the subjobs selected
                        expunge                :  delete completely the subjobs
    
                        You can combine kill and resubmit with '-status <status>' and '-id <id>'.
                        For instance, if you do something like 'masterjob <jobId> -status ERROR_IB resubmit',
                         all the subjobs with status ERROR_IB will be resubmitted
    ```

## **kill**

=== "Usage"
    ```console
    
    usage: kill   <jobId> [<jobId>[,<jobId>]]
    ```

## **w**

=== "Usage"
    ```console
    
    usage: uptime       
    
    options:
    ```

## **uptime**

=== "Usage"
    ```console
    
    usage: uptime       
    
    options:
    ```

=== "Example"
    ```console
    jsh: [alice] > uptime
    137823 running jobs, 161406 waiting jobs, 70 active users
    ```

## **resubmit**

=== "Usage"
    ```console
    
    resubmit: resubmits a job or a group of jobs by IDs
            Usage:
                    resubmit <jobid1> [<jobid2>....]
    ```

## **top**

=== "Usage"
    ```console
    
    usage: top          
    ```

## **df**

=== "Usage"
    ```console
    Shows free disk space
    Usage: df
    ```

## **du**

=== "Usage"
    ```console
    Gives the disk space usge of one or more directories
    Usage: du [-ncs] <path> ...
    
    Options:
    	-n: Print raw numbers in machine readable format
    	-c: Include collections in the summary information
    	-s: Print a summary of all parameters
    ```

=== "Example"
    ```console
    jsh: [alice] > du /alice/cern.ch/user/u/username
    /alice/cern.ch/user/u/username
      Folders: 3
      Files: 2 of an apparent size of 1.925 GB, of which:
      - physical files: 2 files of 3.851 GB with 4 replicas (avg 2 replicas/file), size of one replica: 1.925 GB
    ```

## **fquota**

=== "Usage: jalien"
    ```console
    
    fquota: Displays information about File Quotas.
    usage: fquota   list [-<options>]
    Options:
      -unit = B|K|M|G: unit of file size
    
    fquota set <user> <field> <value> - set the user quota for file catalogue
      (maxNbFiles, maxTotalSize(Byte))
      use <user>=% for all users
    ```

## **jquota**

=== "Usage: jalien"
    ```console
    
    usage: jquota   Displays information about Job Quotas.
    
    options:
                        list [username]        :  get job quota information for the current account, or the indicated one
                        set <user> <field> <value>  :  to set quota fileds (one of  maxUnfinishedJobs, maxTotalCpuCost, maxTotalRunningTime)
    ```

## **listSEs**

=== "Usage"
    ```console
    
    listSEs: print all (or a subset) of the defined SEs with their details
    usage: listSEs   [-qos filter,by,qos] [SE name] [SE name] ...
    
    options:
                        -qos                   :  filter the SEs by the given QoS classes
    ```

## **listSEDistance**

=== "Usage"
    ```console
    
    listSEDistance: Returns the closest working SE for a particular site. Usage
    
    options:
                        -site                  :  site to base the results to, instead of using the default mapping of this client to a site
                        -read                  :  use the read metrics, optionally with an LFN for which to sort the replicas. Default is to print the write metrics.
                        -qos                   :  restrict the returned SEs to this particular tag
    ```

## **setSite**

=== "Usage"
    ```console
    
    usage: setSite   [site name]
    
    options:
    ```

## **testSE**

=== "Usage"
    ```console
    Test the functional status of Grid storage elements
    Usage: testSE [options] <some SE names, numbers or @tags>
                        -v                     :  verbose error messages even when the operation is expected to fail
                        -c                     :  show full command line for each test
                        -t                     :  time each operation
                        -a                     :  test all SEs (obviously a very long operation)
    ```

## **listTransfer**

=== "Usage"
    ```console
    
    listTransfer: returns all the transfers that are waiting in the system
            Usage:
                    listTransfer [-status <status>] [-user <user>] [-id <queueId>] [-verbose] [-master] [-summary] [-all_status] [-jdl] [-destination <site>]  [-list=<number(all transfers by default)>] [-desc]
    ```

## **uuid**

=== "Usage"
    ```console
    
    usage: md5sum   <filename1> [<filename2>] ...
    ```

=== "Example"
    ```console
    jsh: [alice] > uuid example.file
    example.file : created on Thu Oct 08 10:35:24 CEST 2020 (1602146124704) on d8:d3:85:97:6e:c8
    ```

## **stat**

=== "Usage"
    ```console
    
    usage: stat   [-v] <filename1> [<or uuid>] ...
                        -v                     :  More details on the status.
    ```

=== "Example"
    ```console
    jsh: [alice] > stat example.file
    File: /alice/cern.ch/user/u/username/example.file
    Type: f
    Owner: username:username
    Permissions: 755
    Last change: 2020-10-08 10:35:24.0 (1602146124000)
    Size: 56 (56 B)
    MD5: b0b07c5bab5b5418250f8d63d3102726
    GUID: 35eada00-0941-11eb-8158-d8d385976ec8
    	GUID created on Thu Oct 08 10:35:24 CEST 2020 (1602146124704) by d8:d3:85:97:6e:c8
    ```

## **xrdstat**

=== "Usage"
    ```console
    
    usage: xrdstat   [-d [-i]] [-v] [-p PID,PID,...] [-s SE1,SE2,...] [-c] <filename1> [<or UUID>] ...
    
    options:
                        -d                     :  Check by physically downloading each replica and checking its content. Without this a stat (metadata) check is done only.
                        -i                     :  When downloading each replica, ignore `stat` calls and directly try to fetch the content.
                        -s                     :  Comma-separated list of SE names to restrict the checking to. Default is to check all replicas.
                        -c                     :  Print the full command line in case of errors.
                        -v                     :  More details on the status.
                        -p                     :  Comma-separated list of job IDs to check the input data of
                        -o                     :  Only show the online status (for files with tape replicas in particular)
                        -O                     :  Request the file to be brought online
    ```

=== "Example"
    ```console
    jsh: [alice] > xrdstat example.file
    Checking the replicas of /alice/cern.ch/user/u/username/example.file
    	ALICE::RAL::CEPH    	root://alice.echo.stfc.ac.uk:1094/alice:/00/24641/35eada00-0941-11eb-8158-d8d385976ec8	[32mOK[0m
    	ALICE::CERN::EOS    	root://eosalice.cern.ch:1094//00/24641/35eada00-0941-11eb-8158-d8d385976ec8	[32mOK[0m
    ```

## **showTagValue**

=== "Usage"
    ```console
    
    usage: showtagValue   [flags] <filename> [<filename>...]
    
    options:
                        -t                     :  restrict to this (comma separated) tag list only (default is to return all available tags)
                        -c                     :  restrict to these (comma separated) list of attributes
                        -l                     :  list available tags only
    ```

## **time**

=== "Usage: jalien"
    ```console
    Usage: time <times>  <command> [command_arguments] 
    ```

=== "Usage: alien.py"
    ```console
    Command format: time command arguments
    ```

## **commandlist**

=== "Usage"
    ```console
    
    usage: commandlist  
    ```

## **motd**

No help available for this command

## **ping**

=== "Usage: jalien"
    ```console
    
    usage: ping         
                        -c                     :  Number of iterations
                        -s                     :  Sleep between iterations, default 1000 (milliseconds)
                        -n                     :  Numeric IP addresses, don't try to resolve anything
    ```

=== "Usage: alien.py"
    ```console
    ping <count>
    where count is integer
    ```

=== "Example"
    ```console
    jsh: [alice] > ping 5
    Sending 3 messages with a pause of 1000 ms between them
    reply from 2001:1458:201:b50e:0:0:100:18 (alice-jcentral.cern.ch / aliendb9.cern.ch): time=1.058 ms
    reply from 2001:1458:201:b50e:0:0:100:18 (alice-jcentral.cern.ch / aliendb9.cern.ch): time=0.98 ms
    reply from 2001:1458:201:b50e:0:0:100:18 (alice-jcentral.cern.ch / aliendb9.cern.ch): time=1.018 ms
    3 packets transmitted, time 2003 ms
    rtt min/avg/max/mdev = 0.98/1.019/1.058/0.032 ms
    
    Central service endpoint information:
      hostname : aliendb9.cern.ch
    ```

## **whoami**

=== "Usage"
    ```console
    
    usage: whoami       
    
    options:
                        -v                     :  verbose details of the current identity
    ```

=== "Example"
    ```console
    jsh: [alice] > whoami
    username
    ```

## **user**

=== "Usage: jalien"
    ```console
    
    usage: user   <user name>
    
                        Change effective role as specified.
    ```

## **whois**

=== "Usage: jalien"
    ```console
    
    usage: whois   [account name]
    
    options:
                        -s                     :  search for the given string(s) in usernames
                        -f                     :  also search in full names
    ```

## **groups**

=== "Usage"
    ```console
    
    groups [<username>]
    shows the groups current user is a member of.
    ```

=== "Example"
    ```console
    jsh: [alice] > groups
    User: username, main group: username
    Member of groups: username admin 
    ```

## **token**

=== "Usage: jalien"
    ```console
    
    usage: token   [-options]
    
    options:
                        -u <username>          :  switch to another role of yours
                        -v <validity (days)>   :  default depends on token type
                        -t <tokentype>         :  can be one of: job, jobagent, host, user (default)
                        -jobid <job DN extension>  :  expected to be present in a job token
                        -hostname <FQDN>       :  required for a host certificate
    ```

=== "Usage: alien.py"
    ```console
    Print only command!!! Use >token-init< for token (re)generation, see below the arguments
    usage: token   [-options]
    
    options:
                        -u <username>          :  switch to another role of yours
                        -v <validity (days)>   :  default depends on token type
                        -t <tokentype>         :  can be one of: job, jobagent, host, user (default)
                        -jobid <job DN extension>  :  expected to be present in a job token
                        -hostname <FQDN>       :  required for a host certificate
    ```

## **queue**

No help available for this command

## **register**

No help available for this command

## **groupmembers**

=== "Usage"
    ```console
    groupmembers <group> : displays group members 
    ```

